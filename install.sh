#!/bin/sh

install_keyfile() {
    ./scripts/sdsms open key
    install -vm 600 /media/key/luks/keyfile_key /root/
    ./scripts/sdsms close key
}

install_zsh_completion()(
    fpath="/usr/share/zsh/site-functions/"
    script="config/shell/zsh/_sdsms"
    install -Dvm 644 --target-directory="$fpath" "$script"
)

# install all executables:
install -Dvm 755 --target-directory="/usr/local/bin/" scripts/*

# install udev rule
#install -Dvm 644 --target-directory="/etc/udev/rules.d/" config/udev/*

# if zsh is installed; then install completion function:
if command -v zsh 2>/dev/null; then install_zsh_completion; fi

# if keyfile not exists, try to install:
if [ ! -f /root/keyfile_key ]; then install_keyfile; fi
