#!/bin/sh

exists_and_is_block_special()
{
    test -b "$1"
}

get_realpath()
{
    case "$device" in
        /dev/*)  echo "$device" ;;
        LABEL=*) realpath "/dev/disk/by-label/${device#LABEL=}" ;;
        UUID=*)  realpath "/dev/disk/by-uuid/${device#UUID=}" ;;
    esac

}

mount_from_fstab()
{
    dev="/dev/mapper/$1"
    mountpoint=$(awk -v dev="$dev" '!/^#/ && $0 ~ dev {print $2}' /etc/fstab)
    mkdir -p "$mountpoint"
    mount "$dev"
}

awk '!/^[[:space:]]*#/ {print $1, $2, $3}' /etc/crypttab |
while read -r name device keyfile
do
    device=$(get_realpath "$device")

    if exists_and_is_block_special "$device"; then
        cryptsetup open "$device" "$name" --key-file "$keyfile"
    fi

    if exists_and_is_block_special "/dev/mapper/$name"; then
        mount_from_fstab "$name"
    fi

done

