**work in progress**

# Simple Data Storage Management Scripts

These scripts support encryption and backup of a personal data partition by utilizing the features of [btrfs] and [cryptsetup]. In addition, they help to setup the devices to integrate them into the workflow of these scripts.

## Workflow

I store my keys (keyfiles, passphrases, RSA signature etc.) on an encrypted, small, inexpensive usb flash drive and keep duplicates of it. All my personal data is stored on a big ssd built into my laptop. The encrypted data partition is mounted to my home directory if needed. Two encrypted external hard disks coupled in software RAID serve as backup storage and are synchronized with the data partition at regular intervals.

<p align="center">
  <img src="res/workflow.png" />
</p>


## Installation

### Dependencies

* [util-linux] \(findfs, findmnt, mount, umount, lsblk\)
* [coreutils] \(cp, cat, tr, ls, echo, printf, sync, mkdir, rmdir\)
* [grep]
* [cryptsetup]
* [btrfs-progs]


[btrfs]: https://btrfs.wiki.kernel.org/index.php/Main_Page
[cryptsetup]: https://gitlab.com/cryptsetup/cryptsetup
[util-linux]: https://en.wikipedia.org/wiki/Util-linux
[coreutils]: https://www.gnu.org/software/coreutils/
[grep]: https://en.wikipedia.org/wiki/Grep
[btrfs-progs]: https://github.com/kdave/btrfs-progs
